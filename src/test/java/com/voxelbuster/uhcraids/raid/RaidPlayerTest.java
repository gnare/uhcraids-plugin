/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.uhcraids.raid;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

import be.seeseemelk.mockbukkit.MockBukkit;
import com.google.gson.Gson;
import com.voxelbuster.uhcraids.PluginInterface;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import com.voxelbuster.uhcraids.UHCRaidsPlugin;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class RaidPlayerTest {
  @Mock Player mockPlayer;

  @Mock PluginInterface mockPlugin;

  @Mock UHCRaidsPlugin mockInstance;

  @Mock Server mockServer;

  @Mock Inventory mockInventory;

  @Mock FileConfiguration mockConfig;

  @Mock ConfigurationSection mockConfigSection;

  @BeforeEach
  void setUp() {
    when(mockPlugin.getGson()).thenCallRealMethod();
    when(mockPlugin.getServer()).thenReturn(mockServer);
    when(mockPlugin.getConfig()).thenReturn(mockConfig);
    when(mockConfig.getConfigurationSection(anyString())).thenReturn(mockConfigSection);
    when(mockServer.createInventory(any(InventoryHolder.class), anyInt(), anyString()))
        .thenReturn(mockInventory);
    when(mockInventory.getType()).thenReturn(InventoryType.CHEST);
    when(mockInventory.getSize()).thenReturn(27);
    when(mockInventory.getLocation()).thenReturn(null);
    when(mockInventory.getContents())
        .thenReturn(List.of(new ItemStack(Material.COBBLESTONE)).toArray(new ItemStack[0]));

    when(mockPlayer.getName()).thenReturn("TestPlayer");
  }

  @Test
  public void test_setInventorySize_smaller() throws InvalidConfigurationException {
    RaidPlayer rp = new RaidPlayer(mockPlayer, mockPlugin);

    assertTrue(rp.setInventorySize(1));
    verify(mockInventory, times(1)).setContents(any());
  }

  @Test
  public void test_setInventorySize_larger() throws InvalidConfigurationException {
    RaidPlayer rp = new RaidPlayer(mockPlayer, mockPlugin);

    assertTrue(rp.setInventorySize(36));
    verify(mockInventory, times(1)).setContents(any());
  }

  @Test
  public void test_setInventorySize_tooMany() throws InvalidConfigurationException {
    RaidPlayer rp = new RaidPlayer(mockPlayer, mockPlugin);

    assertFalse(rp.setInventorySize(0));
    verify(mockInventory, never()).setContents(any());
  }

  @Test
  public void test_serialize() throws InvalidConfigurationException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
    RaidPlayer rp = new RaidPlayer(mockPlayer, mockPlugin);
    String json = rp.serialize();
    System.out.println(json);
  }
}
