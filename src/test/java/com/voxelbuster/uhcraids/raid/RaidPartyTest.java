/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.uhcraids.raid;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import com.voxelbuster.uhcraids.PluginInterface;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class RaidPartyTest {

  @Mock Player mockPlayer;

  @Mock Player mockPlayer2;

  @Mock PluginInterface mockPlugin;

  @Mock Server mockServer;

  @Mock PluginManager mockPluginManager;

  RaidParty party;

  @BeforeEach
  void setUp() {
    when(mockPlugin.getServer()).thenReturn(mockServer);
    when(mockServer.getPluginManager()).thenReturn(mockPluginManager);

    party = new RaidParty(mockPlayer, mockPlugin);
  }

  @Test
  void test_setLeader() {
    assertFalse(party.setLeader(mockPlayer2));

    party.invitePlayer(mockPlayer2);
    party.acceptInvite(mockPlayer2);

    assertTrue(party.setLeader(mockPlayer2));
    assertEquals(RaidParty.getPartyByLeader(mockPlayer2), party);
  }

  @Test
  void test_invitePlayer() {
    assertFalse(party.tryJoin(mockPlayer2));

    assertTrue(party.invitePlayer(mockPlayer2));

    assertTrue(party.getInvitedPlayers().contains(mockPlayer2));
  }

  @Test
  void test_acceptInvite() {
    assertTrue(party.invitePlayer(mockPlayer2));

    assertTrue(party.acceptInvite(mockPlayer2));

    assertFalse(party.getInvitedPlayers().contains(mockPlayer2));
    assertTrue(party.getPlayers().contains(mockPlayer2));
  }

  @Test
  void test_kickPlayer() {
    party.invitePlayer(mockPlayer2);
    party.acceptInvite(mockPlayer2);

    assertTrue(party.getPlayers().contains(mockPlayer2));
    assertTrue(party.kickPlayer(mockPlayer2));
    assertFalse(party.getPlayers().contains(mockPlayer2));
  }

  @Test
  void test_leave() {
    party.invitePlayer(mockPlayer2);
    party.acceptInvite(mockPlayer2);

    assertTrue(party.getPlayers().contains(mockPlayer2));
    assertTrue(party.leave(mockPlayer2));
    assertFalse(party.getPlayers().contains(mockPlayer2));
  }

  @Test
  void test_tryJoin() {
    assertTrue(party.invitePlayer(mockPlayer2));
    assertTrue(party.tryJoin(mockPlayer2));

    assertTrue(party.getPlayers().contains(mockPlayer2));
  }

  @Test
  void test_disband() {
    party.disband();

    assertTrue(party.getPlayers().isEmpty());
    assertNull(party.getLeader());
    assertFalse(party.isOpen());
  }

  @Test
  void test_getPartyByLeader() {
    assertEquals(party, RaidParty.getPartyByLeader(mockPlayer));
  }

  @Test
  void test_getPlayers() {
    assertTrue(party.invitePlayer(mockPlayer2));
    assertTrue(party.tryJoin(mockPlayer2));

    assertEquals(2, party.getPlayers().size());
    assertTrue(party.getPlayers().contains(mockPlayer));
    assertTrue(party.getPlayers().contains(mockPlayer2));
  }

  @Test
  void test_getInvitedPlayers() {
    assertTrue(party.invitePlayer(mockPlayer2));

    assertEquals(1, party.getPlayers().size());
    assertFalse(party.getInvitedPlayers().contains(mockPlayer));
    assertTrue(party.getInvitedPlayers().contains(mockPlayer2));
  }

  @Test
  void test_getPartiesByDesc() {
    String desc = "test description";
    party.setDescription(desc);

    assertEquals(1, RaidParty.getPartiesByDesc(desc).size());
    assertTrue(RaidParty.getPartiesByDesc(desc).contains(party));
  }
}
