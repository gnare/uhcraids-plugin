/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.uhcraids.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;

public class TestUtils {
  public static String readUrl(String urlString) throws Exception {
    URL url = new URL(urlString);

    try (BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()))) {
      StringBuilder buffer = new StringBuilder();

      int read;
      char[] chars = new char[1024];
      while ((read = reader.read(chars)) != -1) {
        buffer.append(chars, 0, read);
      }

      return buffer.toString();
    }
  }

  public record PaperVersions(
      String project_id,
      String project_name,
      LinkedList<String> version_groups,
      LinkedList<String> versions) {}

  public record PaperBuilds(String project_id, String project_name, LinkedList<String> builds) {}

  public record PaperDownloads(
      String project_id, String project_name, HashMap<String, PaperArtifact> downloads) {}

  public record PaperArtifact(String name, String sha256) {}
}
