/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.uhcraids.integration;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import nl.vv32.rcon.Rcon;
import org.apache.commons.io.FileUtils;

@Log
@RequiredArgsConstructor
public abstract class ServerIT {

  private final String pluginName;
  private final String serverJar;
  private final String mainClass;

  public abstract void installServer();

  public abstract void installPlugin();

  public abstract void installDependencies();

  public void validatePluginLoad() {
    try (Rcon rcon = Rcon.open("localhost", 25575)) {
      rcon.authenticate("testRcon");
      String resp = rcon.sendCommand("plugins");
      assertTrue(resp.contains(pluginName));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public void stopServer() {
    try (Rcon rcon = Rcon.open("localhost", 25575)) {
      rcon.authenticate("testRcon");
      rcon.sendCommand("stop");
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  public void startServer() throws IOException {
    URL[] jars = FileUtils.toURLs(new File(serverJar));

    try (URLClassLoader classLoader =
        new URLClassLoader(jars, this.getClass().getClassLoader().getParent())) {
      FileUtils.writeStringToFile(
          new File("server.properties"),
          "enable-rcon=true\nrcon.password=testRcon\n",
          StandardCharsets.UTF_8);

      Class<?> type = classLoader.loadClass(mainClass);
      Method main = type.getMethod("main", String[].class);
      main.invoke(null, (Object) new String[] {"nogui"});

      boolean canOpenRcon = false;
      while (!canOpenRcon) {
        try (Rcon rcon = Rcon.open("localhost", 25575)) {
          canOpenRcon = true;
        } catch (IOException e) {
          // Ignore, server not ready
        }
        Thread.sleep(2000);
      }
    } catch (ClassNotFoundException
        | IOException
        | NoSuchMethodException
        | IllegalAccessException
        | InvocationTargetException
        | InterruptedException e) {
      log.log(Level.SEVERE, "Caught exception trying to load server jar!", e);
      throw new RuntimeException(e);
    }
  }
}
