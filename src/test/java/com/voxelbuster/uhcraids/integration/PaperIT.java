/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.uhcraids.integration;

import com.google.gson.Gson;
import com.voxelbuster.uhcraids.UHCRaidsPlugin;
import com.voxelbuster.uhcraids.util.TestUtils;
import lombok.extern.java.Log;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.MessageDigest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Log
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PaperIT extends ServerIT {
  private static final String serverJarFilename = "server.jar";
  private static final File pluginJarFile =
      FileUtils.listFiles(new File(".."), new String[]{"jar"}, true).stream()
          .filter(f -> !f.getName().startsWith("original"))
          .filter(f -> f.getName().contains("uhcraids"))
          .findFirst()
          .orElseThrow();

  public PaperIT() {
    super(UHCRaidsPlugin.pluginName, serverJarFilename, "io.papermc.paperclip.Main");
  }

  @Override
  public void installServer() {
    Gson gson = new Gson();
    try {
      String versionsJson = TestUtils.readUrl("https://api.papermc.io/v2/projects/paper/");
      TestUtils.PaperVersions versions = gson.fromJson(versionsJson, TestUtils.PaperVersions.class);

      String latestVersion = versions.versions().peekLast();
      String buildsJson =
          TestUtils.readUrl(
              String.format(
                  "https://api.papermc.io/v2/projects/paper/versions/%s/", latestVersion));
      TestUtils.PaperBuilds builds = gson.fromJson(buildsJson, TestUtils.PaperBuilds.class);

      String latestBuild = builds.builds().peekLast();
      String downloadsJson =
          TestUtils.readUrl(
              String.format(
                  "https://api.papermc.io/v2/projects/paper/versions/%s/builds/%s",
                  latestVersion, latestBuild));
      TestUtils.PaperDownloads downloads =
          gson.fromJson(downloadsJson, TestUtils.PaperDownloads.class);

      TestUtils.PaperArtifact appArtifact = downloads.downloads().get("application");
      String filename = appArtifact.name();
      String sha256 = appArtifact.sha256();

      URL downloadURL =
          new URL(
              String.format(
                  "https://api.papermc.io/v2/projects/paper/versions/%s/builds/%s/downloads/%s",
                  latestVersion, latestBuild, filename));
      log.info("Downloading " + downloadURL + "...");
      File targetFile = new File(serverJarFilename);
      FileUtils.copyURLToFile(downloadURL, targetFile);

      byte[] data = Files.readAllBytes(targetFile.toPath());
      byte[] hash = MessageDigest.getInstance("SHA-256").digest(data);
      String checksum = new BigInteger(1, hash).toString(16);
      assertEquals(sha256, checksum);
      log.info("Download complete.");

      FileUtils.writeStringToFile(new File("eula.txt"), "eula=true", StandardCharsets.UTF_8);

    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void installPlugin() {
    File pluginsDir = new File("plugins");
    File targetPluginFile = new File("plugins/uhcraids.jar");
    //noinspection ResultOfMethodCallIgnored
    pluginsDir.mkdir();
    try {
      FileUtils.copyFile(pluginJarFile, targetPluginFile);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void installDependencies() {
    try {
      FileUtils.copyURLToFile(
          new URL("https://dev.bukkit.org/projects/worldguard/files/latest"),
          new File("plugins/worldguard.jar"));
      FileUtils.copyURLToFile(
          new URL("https://dev.bukkit.org/projects/worldedit/files/latest"),
          new File("plugins/worldedit.jar"));
      FileUtils.copyURLToFile(
          new URL("https://ci.extendedclip.com/job/PlaceholderAPI/174/artifact/build/libs/PlaceholderAPI-2.11.4-DEV-174.jar"),
          new File("plugins/papi.jar"));
      FileUtils.copyURLToFile(
          new URL("https://ci.dmulloy2.net/job/ProtocolLib/lastSuccessfulBuild/artifact/build/libs/ProtocolLib.jar"),
          new File("plugins/protocollib.jar"));
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  @BeforeAll
  public void setUp() throws IOException {
    installServer();
    installPlugin();
    installDependencies();
    startServer();
  }

  @AfterAll
  public void tearDown() {
    stopServer();
  }

  @Test
  public void testPluginLoad() {
    validatePluginLoad();
  }
}
