/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.uhcraids;

import com.comphenix.protocol.ProtocolManager;
import com.google.gson.Gson;
import com.samjakob.spigui.SpiGUI;
import java.io.InputStream;
import java.util.logging.Logger;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Server;
import org.bukkit.configuration.Configuration;

/**
 * An alias interface to be implemented by plugin core classes, used to abstract static calls. Used
 * to increase the usability of mocking in test classes.
 */
public interface PluginInterface {
  default Server getServer() {
    return UHCRaidsPlugin.getInstance().getServer();
  }

  default Configuration getConfig() {
    return UHCRaidsPlugin.getInstance().getConfig();
  }

  default InputStream getResource(String resource) {
    return UHCRaidsPlugin.getInstance().getResource(resource);
  }

  default Logger getLogger() {
    return UHCRaidsPlugin.getInstance().getLogger();
  }

  default SpiGUI getSpiGUI() {
    return UHCRaidsPlugin.getInstance().getSpiGUI();
  }

  default UHCRaidsPlugin.PluginState getPluginState() {
    return UHCRaidsPlugin.getInstance().getPluginState();
  }

  default Permission getPermissions() {
    return UHCRaidsPlugin.getInstance().getPermissions();
  }

  default ProtocolManager getProtocolManager() {
    return UHCRaidsPlugin.getInstance().getProtocolManager();
  }

  default Gson getGson() {
    return UHCRaidsPlugin.getGson();
  }
}
