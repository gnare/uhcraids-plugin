/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.uhcraids.placeholders;

import com.voxelbuster.uhcraids.PluginInterface;
import com.voxelbuster.uhcraids.UHCRaidsPlugin;
import com.voxelbuster.uhcraids.raid.Raid;
import com.voxelbuster.uhcraids.raid.RaidPlayer;
import java.time.Duration;
import java.time.Instant;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.OfflinePlayer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UHCRaidsExpansion extends PlaceholderExpansion {
  @Override
  public @NotNull String getIdentifier() {
    return "uhcr";
  }

  @Override
  public @NotNull String getAuthor() {
    return "VoxelBuster";
  }

  @Override
  public @NotNull String getVersion() {
    return UHCRaidsPlugin.getInstance().getDescription().getVersion();
  }

  @Override
  public @Nullable String onRequest(OfflinePlayer player, @NotNull String params) {
    if (params.equalsIgnoreCase("border-time")) {
      RaidPlayer raidPlayer = plugin.getPluginState().getRaidPlayersMap().get(player);
      if (raidPlayer != null) {
        if (raidPlayer.getTimeOutOfBounds() != null) {
          double timeS = plugin.getConfig().getDouble("raid-border.grace-period", 5) -
              (double) Duration.between(raidPlayer.getTimeOutOfBounds(), Instant.now()).toMillis() / 1000.0;
          return String.format("%.1f", Math.max(timeS, 0));
        }
      }
      return "null";
    } else if (params.equalsIgnoreCase("after-death-time")) {
      RaidPlayer raidPlayer = UHCRaidsPlugin.getInstance().getPluginState().getRaidPlayersMap().get(player);
      if (raidPlayer != null) {
        if (raidPlayer.getTimeOfDeath() != null) {
          double timeS = plugin.getConfig().getDouble("raid-death.default-time", 5) -
              (double) Duration.between(raidPlayer.getTimeOfDeath(), Instant.now()).toMillis() / 1000.0;
          return String.format("%.1f", Math.max(timeS, 0));
        }
      }
      return "null";
    } else if (params.equalsIgnoreCase("exit-time")) {
      RaidPlayer raidPlayer = UHCRaidsPlugin.getInstance().getPluginState().getRaidPlayersMap().get(player);
      if (raidPlayer != null) {
        if (raidPlayer.getActiveTimedTask() != null && raidPlayer.getActiveTimedTask().getName().equals("exitTimer")) {
          double timeS = (double) Duration.between(Instant.now(),
              raidPlayer.getActiveTimedTask().getTaskTime().toInstant()).toMillis() / 1000.0;
            return String.format("%.1f", Math.max(timeS, 0));
        }
      }
      return "null";
    } else if (params.equalsIgnoreCase("raid-start-countdown")) {
      RaidPlayer raidPlayer =
          UHCRaidsPlugin.getInstance().getPluginState().getRaidPlayersMap().get(player);
      if (raidPlayer != null) {
        Raid raid = raidPlayer.getCurrentRaid();
        if (raid != null) {
          Instant startTime = raid.getStartTime();
          Instant now = Instant.now();
          double timeS = Duration.between(now, startTime).toMillis() / 1000.0;
          return String.format("%.1f", Math.max(timeS, 0));
        }
      }

      return "null";
    }

    return super.onRequest(player, params);
  }

  @Getter(AccessLevel.NONE)
  @Setter(AccessLevel.NONE)
  private transient PluginInterface plugin = pluginImpl;

  private static final PluginInterface pluginImpl = new PluginInterface() {
  };
}
