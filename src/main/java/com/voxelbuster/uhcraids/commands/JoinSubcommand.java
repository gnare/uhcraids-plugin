/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.uhcraids.commands;

import com.samjakob.spigui.buttons.SGButton;
import com.samjakob.spigui.item.ItemBuilder;
import com.samjakob.spigui.menu.SGMenu;
import com.voxelbuster.uhcraids.PluginInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lombok.Getter;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class JoinSubcommand extends PluginSubcommand {

  @Getter
  private final SGMenu raidLocationMenu = getPlugin()
      .getSpiGUI()
      .create(NamedTextColor.DARK_PURPLE.value() + "Choose Raid Location", 1);

  protected JoinSubcommand(PluginInterface plugin) {
    super("join", "uhcraids.join", plugin);
    this.aliases = List.of("join", "j", "enter", "play");
    this.description = "Join a raid";
    this.usage = NamedTextColor.GREEN.value() + "/uhcraids join " + NamedTextColor.GOLD.value() + " [map] [options]";

    raidLocationMenu.setRowsPerPage(1);
    raidLocationMenu.setAutomaticPaginationEnabled(true);
  }

  @Override
  protected boolean execute(CommandSender commandSender, String alias, String[] args) {
    if (aliases.contains(alias.toLowerCase())) {
      if (commandSender instanceof Player player) {
        if (args.length < 1) {
          player.openInventory(raidLocationMenu.getInventory());
          return true;

        } else if (args.length == 1) {
          if (getPlugin().getPluginState().getRaidSettingsMap().containsKey(args[0])) {
            // TODO start matchmaking
            return true;
          } else {
            commandSender.sendMessage(NamedTextColor.RED.value() + "That is not a valid raid location.");
            return false;
          }
        } else {
          if (getPlugin().getPluginState().getRaidSettingsMap().containsKey(args[0])) {
            // TODO start matchmaking with options
            return true;
          } else {
            commandSender.sendMessage(NamedTextColor.RED.value() + "That is not a valid raid location.");
            return false;
          }
        }
      } else {
        commandSender.sendMessage(NamedTextColor.RED.value() + "You must be a player to use this command!");
        return false;
      }
    }
    return false;
  }

  @Override
  public List<String> tabComplete(CommandSender sender, String alias, String[] args)
      throws IllegalArgumentException {
    if (aliases.contains(alias.toLowerCase())) {
      if (args.length < 1) {
        return new ArrayList<>(getPlugin().getPluginState().getRaidSettingsMap().keySet());
      } else if (args.length == 1) {
        return getPlugin().getPluginState().getRaidSettingsMap().keySet().stream()
            .filter(s -> s.contains(args[0]))
            .toList();
      } else {
        if (getPlugin().getPluginState().getRaidSettingsMap().containsKey(args[0])) {
          return List.of("night", "day"); // TODO autofill raid options
        } else {
          return Collections.emptyList();
        }
      }
    }
    return Collections.emptyList();
  }

  public void updateMenu() {
    getPlugin().getPluginState().getRaidSettingsMap().values().stream()
        .map(
            rs ->
                new SGButton(
                    new ItemBuilder(rs.getRaidMap().getWorldIcon())
                        .name(rs.getRaidMap().getName())
                        .lore(
                            "Max players: " + NamedTextColor.YELLOW.value() + rs.getMaxPlayers(),
                            String.format(
                                "%sMax raid time: %s%02d:%02d",
                                NamedTextColor.WHITE.value(),
                                NamedTextColor.YELLOW.value(),
                                rs.getMaxRaidTime() / 60,
                                rs.getMaxRaidTime() % 60),
                            "Friendly Fire: "
                                + (rs.isFriendlyFireEnabled()
                                ? NamedTextColor.RED.value() + "ON"
                                : NamedTextColor.GREEN.value() + "OFF"))
                        .build())
                    .withListener(event -> {}))
        .forEach(raidLocationMenu::addButton);
  }
}
