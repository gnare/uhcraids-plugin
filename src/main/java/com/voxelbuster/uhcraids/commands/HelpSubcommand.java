/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.uhcraids.commands;

import com.voxelbuster.uhcraids.PluginInterface;
import java.util.List;
import org.bukkit.command.CommandSender;

public class HelpSubcommand extends PluginSubcommand {
  protected HelpSubcommand(PluginInterface plugin) {
    super("help", "uhcraids.help", plugin);
    this.aliases = List.of("help", "h");
    this.description = "UHCRaids help command.";
  }

  @Override
  protected boolean execute(CommandSender commandSender, String alias, String[] args) {
    return false;
  }

  @Override
  public List<String> tabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
    return null;
  }
}
