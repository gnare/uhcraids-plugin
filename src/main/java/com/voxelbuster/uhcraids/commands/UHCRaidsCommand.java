/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.uhcraids.commands;

import com.voxelbuster.uhcraids.PluginInterface;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class UHCRaidsCommand extends Command implements CommandExecutor, TabCompleter {
  private static final PluginInterface pluginImpl = new PluginInterface() {};

  List<String> commandAliases = Arrays.asList("uhcraids", "uhcr", "raids");

  @Getter(AccessLevel.NONE)
  @Setter(AccessLevel.NONE)
  private transient PluginInterface plugin;

  public UHCRaidsCommand(@NotNull String name) {
    this(name, pluginImpl);
  }

  protected UHCRaidsCommand(@NotNull String name, PluginInterface plugin) {
    super(name);
    this.plugin = plugin;
    this.description =
        NamedTextColor.AQUA.value()
            + "Command that contains all functionality of UHCRaids"
            + NamedTextColor.GREEN.value()
            + " /uhcraids"
            + NamedTextColor.AQUA.value()
            + " or"
            + NamedTextColor.GREEN.value()
            + " /uhcraids help to see subcommands.";
    this.usageMessage = NamedTextColor.GREEN.value() + "/uhcraids <subcommand>";
    this.setAliases(commandAliases);
  }

  @Override
  public boolean onCommand(
      @NotNull CommandSender commandSender,
      @NotNull Command command,
      @NotNull String alias,
      @NotNull String[] args) {
    return execute(commandSender, alias, args);
  }

  @Override
  public boolean execute(
      @NotNull CommandSender commandSender, @NotNull String alias, @NotNull String[] args) {
    if (commandAliases.contains(alias.toLowerCase())) {
      if (args.length > 0) {
        PluginSubcommand subcommandInstance;
        switch (Objects.requireNonNull(SubCommand.subCommandByAlias(args[0].toLowerCase()))) {
          case JOIN -> subcommandInstance = new JoinSubcommand(plugin);
          case NONE -> {
            args = new String[]{"help"};
            subcommandInstance = new HelpSubcommand(plugin);
          }
          case HELP -> subcommandInstance = new HelpSubcommand(plugin);
          default -> {
            commandSender.sendMessage(
                NamedTextColor.RED.value()
                    + "Invalid subcommand. "
                    + NamedTextColor.GREEN.value()
                    + "/uhcraids help "
                    + NamedTextColor.RED.value()
                    + "for help.");
            return false;
          }
        }

        if (subcommandInstance.hasPermission(commandSender)) {
          return subcommandInstance.execute(
              commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
        } else {
          commandSender.sendMessage(
              NamedTextColor.RED + "You do not have permission to use that command.");
          return false;
        }
      } else {
        commandSender.sendMessage(
            NamedTextColor.RED.value()
                + "Invalid subcommand. "
                + NamedTextColor.GREEN.value()
                + "/uhcraids help "
                + NamedTextColor.RED.value()
                + "for help.");
        return false;
      }
    }
    return false;
  }

  @Nullable
  @Override
  public List<String> onTabComplete(
      @NotNull CommandSender commandSender,
      @NotNull Command command,
      @NotNull String alias,
      @NotNull String[] args) {
    return tabComplete(commandSender, alias, args);
  }

  @Override
  public @NotNull List<String> tabComplete(
      @NotNull CommandSender commandSender, String alias, String[] args)
      throws IllegalArgumentException {
    if (commandAliases.contains(alias.toLowerCase())) {
      if (args.length == 1) {
        return SubCommand.getAllAliases().stream()
            .filter(s -> s.startsWith(args[0].toLowerCase()))
            .collect(Collectors.toList());
      } else {
        return switch (Objects.requireNonNull(
            SubCommand.subCommandByAlias(args[0].toLowerCase()))) {
          case JOIN -> new JoinSubcommand(plugin)
              .tabComplete(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
          case NONE -> Collections.emptyList();
          case HELP -> new HelpSubcommand(plugin)
              .tabComplete(commandSender, args[0], Arrays.copyOfRange(args, 1, args.length));
        };
      }
    } else {
      return super.tabComplete(commandSender, alias, args);
    }
  }

  enum SubCommand {
    NONE(HelpSubcommand.class, "", null),
    HELP(HelpSubcommand.class, "help", "h"),
    JOIN(JoinSubcommand.class, "join", "j", "enter", "play");

    private final List<String> aliases;

    private final Class<? extends PluginSubcommand> pluginSubcommandClass;

    SubCommand(Class<? extends PluginSubcommand> pluginSubcommandClass, String... aliases) {
      this.pluginSubcommandClass = pluginSubcommandClass;
      this.aliases = Arrays.asList(aliases);
    }

    public static SubCommand subCommandByAlias(String s) {
      for (SubCommand subCommand : values()) {
        if (subCommand.hasAlias(s)) {
          return subCommand;
        }
      }
      return NONE;
    }

    public boolean hasAlias(String s) {
      return this.aliases.contains(s);
    }

    public static ArrayList<String> getAllAliases() {
      ArrayList<String> aliases = new ArrayList<>();
      for (SubCommand subCommand : values()) {
        aliases.addAll(
            subCommand.aliases.stream()
                .filter(s -> s != null && !s.equals(""))
                .collect(Collectors.toCollection(ArrayList::new)));
      }
      return aliases;
    }

    public Class<? extends PluginSubcommand> getSubcommand() {
      return pluginSubcommandClass;
    }
  }
}
