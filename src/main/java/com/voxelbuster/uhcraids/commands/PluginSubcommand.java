/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.uhcraids.commands;

import com.voxelbuster.uhcraids.PluginInterface;
import java.util.List;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.command.CommandSender;

@Getter
public abstract class PluginSubcommand {
  protected final String name;

  protected final String commandPermission;

  protected String description, usage;
  protected List<String> aliases;

  protected PluginSubcommand(String name, String commandPermission, PluginInterface plugin) {
    this.name = name;
    this.commandPermission = commandPermission;
    this.plugin = plugin;
  }

  protected abstract boolean execute(CommandSender commandSender, String alias, String[] args);

  public abstract List<String> tabComplete(CommandSender sender, String alias, String[] args)
      throws IllegalArgumentException;

  public void sendUsageMessage(CommandSender sender) {
    sender.sendMessage(NamedTextColor.GOLD + "Usage: " + NamedTextColor.GREEN + usage);
  }

  @Getter(AccessLevel.PACKAGE)
  @Setter(AccessLevel.NONE)
  private transient PluginInterface plugin;

  private static final PluginInterface pluginImpl = new PluginInterface() {};

  public boolean hasPermission(CommandSender commandSender) {
    if (plugin.getPermissions() != null) {
      return plugin.getPermissions().has(commandSender, commandPermission);
    } else {
      return commandSender.hasPermission(commandPermission);
    }
  }
}
