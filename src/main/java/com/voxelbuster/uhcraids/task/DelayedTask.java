/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.uhcraids.task;

import com.google.common.util.concurrent.MoreExecutors;
import com.sk89q.worldedit.util.task.AbstractTask;
import com.sk89q.worldedit.util.task.progress.Progress;
import com.voxelbuster.uhcraids.PluginInterface;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
public class DelayedTask extends AbstractTask<Void> {

  private final Date taskTime;
  private final Runnable task;
  private final Runnable cancelTask;

  @Getter(AccessLevel.NONE)
  private final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

  public DelayedTask(String name, @Nullable Object owner, Date taskTime, Runnable task) {
    this(name, owner, taskTime, task, () -> {});
  }

  public DelayedTask(
      String name, @Nullable Object owner, Date taskTime, Runnable task, Runnable cancelTask) {
    this(name, owner, pluginImpl, taskTime, task, cancelTask);
  }

  protected DelayedTask(
      String name,
      @Nullable Object owner,
      PluginInterface plugin,
      Date taskTime,
      Runnable task,
      Runnable cancelTask) {
    super(name, owner);
    this.plugin = plugin;
    this.taskTime = taskTime;
    this.task = task;
    this.cancelTask = cancelTask;

    this.addListener(task, MoreExecutors.directExecutor());
  }

  @Override
  public State getState() {
    if (isCancelled()) {
      return State.CANCELLED;
    }

    if (Instant.now().isBefore(taskTime.toInstant())) {
      return State.SCHEDULED;
    }

    if (isDone()) {
      return State.SUCCEEDED;
    }

    return State.RUNNING;
  }

  @Override
  public Void get() throws InterruptedException, ExecutionException {
    while (Instant.now().isBefore(taskTime.toInstant())) {
      synchronized (taskTime) {
        executor.schedule(
            () -> {
              synchronized (taskTime) {
                taskTime.notifyAll();
              }
            },
            Duration.between(Instant.now(), taskTime.toInstant()).toMillis(),
            TimeUnit.MILLISECONDS);
        taskTime.wait();
      }
    }
    return super.get();
  }

  public void start() {
    new Thread(
            () -> {
              try {
                get();
              } catch (InterruptedException | ExecutionException e) {
                if (!isCancelled()) { // This should only be interrupted if the task was cancelled.
                  throw new RuntimeException(e);
                }
              }
            })
        .start();
  }

  /** Abort the task immediately. */
  public boolean cancel() {
    boolean result = super.cancel(true);
    executor.shutdownNow();
    cancelTask.run();
    return result;
  }

  @Override
  public Progress getProgress() {
    Duration max = Duration.between(getCreationDate().toInstant(), getTaskTime().toInstant());
    Duration current = Duration.between(getCreationDate().toInstant(), Instant.now());
    return Progress.of((double) current.toMillis() / (double) max.toMillis());
  }

  @Getter(AccessLevel.NONE)
  @Setter(AccessLevel.NONE)
  private transient PluginInterface plugin = pluginImpl;

  private static final PluginInterface pluginImpl = new PluginInterface() {};
}
