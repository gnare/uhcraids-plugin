/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.uhcraids.raid.skills;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Set;
import java.util.stream.IntStream;
import lombok.Getter;

@Getter
public class TableLevellingPattern extends LevellingPattern {
  private final Hashtable<Integer, LevellingPattern> levelTable = new Hashtable<>();

  public TableLevellingPattern(Map<Integer, LevellingPattern> table) {
    super(PatternType.TABLE);
    this.levelTable.putAll(table);

    if (!propagateTableToLevel(100)) {
      throw new IllegalStateException("Table must at least have a level 1 pattern!");
    }
  }

  public boolean propagateTableToLevel(int level) {
    if (levelTable.isEmpty() || levelTable.get(1) == null) {
      return false;
    }

    // Copy the set, so we don't inflate the stream operation
    Set<Integer> keySet = new HashSet<>(levelTable.keySet());

    IntStream.range(1, level)
        .forEach(
            levelKey -> levelTable.put(
                levelKey,
                levelTable.get(
                    keySet.stream()
                        .filter(key -> key <= levelKey)
                        .max(Integer::compareTo)
                        .orElse(1))));

    return true;
  }

  @Override
  public int getExpForNextLevel(int currentLevel) {
    return this.levelTable.get(currentLevel).getExpForNextLevel(currentLevel);
  }

  @Override
  public int getTotalExp(int level) {
    return this.levelTable.get(level).getTotalExp(level);
  }
}
