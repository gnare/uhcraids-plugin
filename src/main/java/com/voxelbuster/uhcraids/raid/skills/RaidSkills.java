/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.uhcraids.raid.skills;

import com.voxelbuster.uhcraids.persistence.IPersistentPluginData;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class RaidSkills implements IPersistentPluginData {
  private int enduranceLv,
      enduranceExp,
      strengthLv,
      strengthExp,
      archeryLv,
      archeryExp,
      vitalityLv,
      vitalityExp,
      metabolismLv,
      metabolismExp,
      acrobaticsLv,
      acrobaticsExp,
      shieldsLv,
      shieldsExp,
      alchemyLv,
      alchemyExp,
      immunityLv,
      immunityExp,
      tradingLv,
      tradingExp,
      enchantingLv,
      enchantingExp,
      craftingLv,
      craftingExp,
      sneakLv,
      sneakExp,
      fishingLv,
      fishingExp,
      cookingLv,
      cookingExp,
      husbandryLv,
      husbandryExp,
      gardeningLv,
      gardeningExp,
      dexterityLv,
      dexterityExp,
      repairLv,
      repairExp,
      medicalLv,
      medicalExp;

  public static RaidSkills getStartingSkills() {
    return new RaidSkills(
        1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1,
        0, 1, 0, 1, 0, 1, 0, 1, 0);
  }

  private void setSkillLevel(SkillType type, int level) {
    switch (type) {
      case ENDURANCE:
        this.enduranceLv = level;
      case STRENGTH:
        this.strengthLv = level;
      case ARCHERY:
        this.archeryLv = level;
      case VITALITY:
        this.vitalityLv = level;
      case METABOLISM:
        this.metabolismLv = level;
      case ACROBATICS:
        this.acrobaticsLv = level;
      case SHIELDS:
        this.shieldsLv = level;
      case ALCHEMY:
        this.alchemyLv = level;
      case IMMUNITY:
        this.immunityLv = level;
      case TRADING:
        this.tradingLv = level;
      case ENCHANTING:
        this.enchantingLv = level;
      case CRAFTING:
        this.craftingLv = level;
      case SNEAK:
        this.sneakLv = level;
      case FISHING:
        this.fishingLv = level;
      case COOKING:
        this.cookingLv = level;
      case HUSBANDRY:
        this.husbandryLv = level;
      case GARDENING:
        this.gardeningLv = level;
      case DEXTERITY:
        this.dexterityLv = level;
      case REPAIR:
        this.repairLv = level;
      case MEDICAL:
        this.medicalLv = level;
    }
  }

  private void setSkillExp(SkillType type, int exp) {
    switch (type) {
      case ENDURANCE:
        this.enduranceExp = exp;
      case STRENGTH:
        this.strengthExp = exp;
      case ARCHERY:
        this.archeryExp = exp;
      case VITALITY:
        this.vitalityExp = exp;
      case METABOLISM:
        this.metabolismExp = exp;
      case ACROBATICS:
        this.acrobaticsExp = exp;
      case SHIELDS:
        this.shieldsExp = exp;
      case ALCHEMY:
        this.alchemyExp = exp;
      case IMMUNITY:
        this.immunityExp = exp;
      case TRADING:
        this.tradingExp = exp;
      case ENCHANTING:
        this.enchantingExp = exp;
      case CRAFTING:
        this.craftingExp = exp;
      case SNEAK:
        this.sneakExp = exp;
      case FISHING:
        this.fishingExp = exp;
      case COOKING:
        this.cookingExp = exp;
      case HUSBANDRY:
        this.husbandryExp = exp;
      case GARDENING:
        this.gardeningExp = exp;
      case DEXTERITY:
        this.dexterityExp = exp;
      case REPAIR:
        this.repairExp = exp;
      case MEDICAL:
        this.medicalExp = exp;
    }
  }

  public void addSkillExp(SkillType type, int exp) {} // TODO

  public int getExpToNextLevel(SkillType type) {
    return 0;
  } // TODO

  @SuppressWarnings("DuplicatedCode")
  public int getSkillLevel(SkillType type) {
    return switch (type) {
      case ENDURANCE -> enduranceLv;
      case STRENGTH -> strengthLv;
      case ARCHERY -> archeryLv;
      case VITALITY -> vitalityLv;
      case METABOLISM -> metabolismLv;
      case ACROBATICS -> acrobaticsLv;
      case SHIELDS -> shieldsLv;
      case ALCHEMY -> alchemyLv;
      case IMMUNITY -> immunityLv;
      case TRADING -> tradingLv;
      case ENCHANTING -> enchantingLv;
      case CRAFTING -> craftingLv;
      case SNEAK -> sneakLv;
      case FISHING -> fishingLv;
      case COOKING -> cookingLv;
      case HUSBANDRY -> husbandryLv;
      case GARDENING -> gardeningLv;
      case DEXTERITY -> dexterityLv;
      case REPAIR -> repairLv;
      case MEDICAL -> medicalLv;
    };
  }

  @SuppressWarnings("DuplicatedCode")
  public int getSkillExp(SkillType type) {
    return switch (type) {
      case ENDURANCE -> enduranceExp;
      case STRENGTH -> strengthExp;
      case ARCHERY -> archeryExp;
      case VITALITY -> vitalityExp;
      case METABOLISM -> metabolismExp;
      case ACROBATICS -> acrobaticsExp;
      case SHIELDS -> shieldsExp;
      case ALCHEMY -> alchemyExp;
      case IMMUNITY -> immunityExp;
      case TRADING -> tradingExp;
      case ENCHANTING -> enchantingExp;
      case CRAFTING -> craftingExp;
      case SNEAK -> sneakExp;
      case FISHING -> fishingExp;
      case COOKING -> cookingExp;
      case HUSBANDRY -> husbandryExp;
      case GARDENING -> gardeningExp;
      case DEXTERITY -> dexterityExp;
      case REPAIR -> repairExp;
      case MEDICAL -> medicalExp;
    };
  }

  public enum SkillType {
    ENDURANCE,
    STRENGTH,
    ARCHERY,
    VITALITY,
    METABOLISM,
    ACROBATICS,
    SHIELDS,
    ALCHEMY,
    IMMUNITY,
    TRADING,
    ENCHANTING,
    CRAFTING,
    SNEAK,
    FISHING,
    COOKING,
    HUSBANDRY,
    GARDENING,
    DEXTERITY,
    REPAIR,
    MEDICAL
  }
}
