/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.uhcraids.raid.skills;

import lombok.Getter;

@Getter
public class LinearLevellingPattern extends LevellingPattern {
  private final int baseExp, expIncreasePerLevel;

  public LinearLevellingPattern(int baseExp, int expIncreasePerLevel) {
    super(PatternType.LINEAR);
    this.baseExp = baseExp;
    this.expIncreasePerLevel = expIncreasePerLevel;
  }

  @Override
  public int getExpForNextLevel(int currentLevel) {
    return expIncreasePerLevel * currentLevel + baseExp;
  }

  @Override
  public int getTotalExp(int level) {
    int sum = 0;
    for (int i = 0; i < level; i++) {
      sum += getExpForNextLevel(i);
    }
    return sum;
  }
}
