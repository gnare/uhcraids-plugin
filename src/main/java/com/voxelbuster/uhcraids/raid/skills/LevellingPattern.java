/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.uhcraids.raid.skills;

import lombok.Getter;

@Getter
public abstract class LevellingPattern {
  private final PatternType type;

  protected LevellingPattern(PatternType type) {
    this.type = type;
  }

  public abstract int getExpForNextLevel(int currentLevel);

  public int getExpForNextLevel(int currentLevel, int exp) {
    return getExpForNextLevel(currentLevel) - exp;
  }

  public abstract int getTotalExp(int level);

  public int getTotalExp(int level, int exp) {
    return getTotalExp(level) + exp;
  }

  public enum PatternType {
    CONSTANT,
    LINEAR,
    QUADRATIC,
    TABLE
  }
}
