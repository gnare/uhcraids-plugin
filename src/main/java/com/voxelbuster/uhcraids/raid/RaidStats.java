/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.uhcraids.raid;

import com.voxelbuster.uhcraids.persistence.IPersistentPluginData;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter(value = AccessLevel.PACKAGE)
public class RaidStats implements IPersistentPluginData {
    private int playerKills = 0;
    private int mobKills = 0;
    private int npcKills = 0;
    private int creatureKills = 0;
    private int itemsLooted = 0;
    private int bossKills = 0;
    private int teamKills = 0;
    private int meleeKills = 0;
    private int rangedKills = 0;
    private int environmentKills = 0;
    private int explosiveKills = 0;
    private int magicKills = 0;
    private int teamAssists = 0;
    private int arrowsFired = 0;
    private int arrowsHit = 0;
    private int longshots = 0;
    private int raids = 0;
    private int raidsSurvived = 0;
    private int raidsAbandoned = 0;
    private int deaths = 0;

    private int killStreak = 0;
    private int survivalStreak = 0;

    private double longestShotDistance = 0d;
    private double healthLost = 0d;
    private double healthHealed = 0d;
    private double damageDealtToPlayers = 0d;
    private double damageDealtToNpcs = 0d;
    private double timePlayed = 0d;

    public float getKDR() {
        return (float) (playerKills + mobKills + npcKills) / (deaths + raidsAbandoned);
    }

    public float getPlayerKDR() {
        return (float) playerKills / (deaths + raidsAbandoned);
    }

    public float getSurvivalRate() {
        return (float) raidsSurvived / raids;
    }

    public float getLeaveRate() {
        return (float) raidsAbandoned / raids;
    }

    public double getAverageTimePerRaid() {
        return timePlayed / raids;
    }

    public float getRangedAccuracy() {
        return (float) arrowsHit / arrowsFired;
    }
    
    public double offerLongshotDistance(double longestShotDistance) {
        this.longestShotDistance = Math.max(longestShotDistance, this.longestShotDistance);
        return this.longestShotDistance;
    }

    public void addPlayerKills(int playerKills) {
        this.playerKills += playerKills;
    }

    public void addMobKills(int mobKills) {
        this.mobKills += mobKills;
    }

    public void addNpcKills(int npcKills) {
        this.npcKills += npcKills;
    }

    public void addCreatureKills(int creatureKills) {
        this.creatureKills += creatureKills;
    }

    public void addItemsLooted(int itemsLooted) {
        this.itemsLooted += itemsLooted;
    }

    public void addBossKills(int bossKills) {
        this.bossKills += bossKills;
    }

    public void addTeamKills(int teamKills) {
        this.teamKills += teamKills;
    }

    public void addMeleeKills(int meleeKills) {
        this.meleeKills += meleeKills;
    }

    public void addRangedKills(int rangedKills) {
        this.rangedKills += rangedKills;
    }

    public void addEnvironmentKills(int environmentKills) {
        this.environmentKills += environmentKills;
    }

    public void addExplosiveKills(int explosiveKills) {
        this.explosiveKills += explosiveKills;
    }

    public void addMagicKills(int magicKills) {
        this.magicKills += magicKills;
    }

    public void addTeamAssists(int teamAssists) {
        this.teamAssists += teamAssists;
    }

    public void addArrowsFired(int arrowsFired) {
        this.arrowsFired += arrowsFired;
    }

    public void addArrowsHit(int arrowsHit) {
        this.arrowsHit += arrowsHit;
    }

    public void addLongshots(int longshots) {
        this.longshots += longshots;
    }

    public void addRaids(int raids) {
        this.raids += raids;
    }

    public void addRaidsSurvived(int raidsSurvived) {
        this.raidsSurvived += raidsSurvived;
    }

    public void addRaidsAbandoned(int raidsAbandoned) {
        this.raidsAbandoned += raidsAbandoned;
    }

    public void addDeaths(int deaths) {
        this.deaths += deaths;
    }

    public void addKillStreak(int killStreak) {
        this.killStreak += killStreak;
    }

    public void addSurvivalStreak(int survivalStreak) {
        this.survivalStreak += survivalStreak;
    }

    public void addHealthLost(double healthLost) {
        this.healthLost += healthLost;
    }

    public void addHealthHealed(double healthHealed) {
        this.healthHealed += healthHealed;
    }

    public void addDamageDealtToPlayers(double damageDealtToPlayers) {
        this.damageDealtToPlayers += damageDealtToPlayers;
    }

    public void addDamageDealtToNpcs(double damageDealtToNpcs) {
        this.damageDealtToNpcs += damageDealtToNpcs;
    }

    public void addTimePlayed(double timePlayed) {
        this.timePlayed += timePlayed;
    }

    public void resetKillStreak() {
        this.killStreak = 0;
    }

    public void resetSurvivalStreak() {
        this.survivalStreak = 0;
    }
}
