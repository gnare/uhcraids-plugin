/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.uhcraids.raid;

import com.voxelbuster.uhcraids.PluginInterface;
import com.voxelbuster.uhcraids.config.types.WeatherType;
import com.voxelbuster.uhcraids.persistence.IPersistentPluginData;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.configuration.ConfigurationSection;

@Getter
public class RaidSettings implements IPersistentPluginData {
  private final RaidMap raidMap;
  private final int maxRaidTime, raidStartCountdown, maxPlayers;
  private final boolean blindDuringCountdownEnabled,
      friendlyFireEnabled,
      taggedModeEnabled,
      obituariesEnabled,
      undeadBurnEnabled,
      dayNightCycleEnabled,
      bossesEnabled,
      passiveMobsEnabled,
      globalAggroEnabled;

  private final Set<WeatherType> enabledWeatherTypes;

  private final ConfigurationSection configSection;

  /**
    Permission string to follow <code>uhcraids.join.raid.[subnode]</code>, that a player must have to join.
   */
  private final String joinPermissionSubnode;

  @Getter(AccessLevel.NONE)
  @Setter(AccessLevel.NONE)
  private transient PluginInterface plugin;

  private static final PluginInterface pluginImpl = new PluginInterface() {};

  @lombok.Builder(
      toBuilder = true,
      builderClassName = "RaidSettingsInternalBuilder",
      builderMethodName = "internalBuilder")
  RaidSettings(
      RaidMap raidMap,
      int maxRaidTime,
      int raidStartCountdown,
      int maxPlayers,
      boolean blindDuringCountdownEnabled,
      boolean friendlyFireEnabled,
      boolean taggedModeEnabled,
      boolean obituariesEnabled,
      boolean undeadBurnEnabled,
      boolean dayNightCycleEnabled,
      boolean bossesEnabled,
      boolean passiveMobsEnabled,
      boolean globalAggroEnabled,
      Set<WeatherType> enabledWeatherTypes,
      String joinPermissionSubnode) {
    this(
        raidMap,
        maxRaidTime,
        raidStartCountdown,
        maxPlayers,
        blindDuringCountdownEnabled,
        friendlyFireEnabled,
        taggedModeEnabled,
        obituariesEnabled,
        undeadBurnEnabled,
        dayNightCycleEnabled,
        bossesEnabled,
        passiveMobsEnabled,
        globalAggroEnabled,
        enabledWeatherTypes,
        joinPermissionSubnode,
        pluginImpl);
  }

  RaidSettings(
      RaidMap raidMap,
      int maxRaidTime,
      int raidStartCountdown,
      int maxPlayers,
      boolean blindDuringCountdownEnabled,
      boolean friendlyFireEnabled,
      boolean taggedModeEnabled,
      boolean obituariesEnabled,
      boolean undeadBurnEnabled,
      boolean dayNightCycleEnabled,
      boolean bossesEnabled,
      boolean passiveMobsEnabled,
      boolean globalAggroEnabled,
      Set<WeatherType> enabledWeatherTypes,
      String joinPermissionSubnode,
      PluginInterface plugin) {
    this.raidMap = raidMap;
    this.maxRaidTime = maxRaidTime;
    this.maxPlayers = maxPlayers;
    this.raidStartCountdown = raidStartCountdown;
    this.blindDuringCountdownEnabled = blindDuringCountdownEnabled;
    this.friendlyFireEnabled = friendlyFireEnabled;
    this.taggedModeEnabled = taggedModeEnabled;
    this.obituariesEnabled = obituariesEnabled;
    this.undeadBurnEnabled = undeadBurnEnabled;
    this.dayNightCycleEnabled = dayNightCycleEnabled;
    this.bossesEnabled = bossesEnabled;
    this.passiveMobsEnabled = passiveMobsEnabled;
    this.globalAggroEnabled = globalAggroEnabled;
    this.enabledWeatherTypes = enabledWeatherTypes;
    this.joinPermissionSubnode = joinPermissionSubnode;
    this.plugin = plugin;

    this.configSection = plugin.getConfig().getConfigurationSection("raid-general");
  }

  public static Builder builder() {
    return new Builder();
  }

  public static class Builder extends RaidSettingsInternalBuilder {
    Builder() {
      super();
      ConfigurationSection raidGeneralSection =
          Objects.requireNonNull(pluginImpl.getConfig().getConfigurationSection("raid-general"));

      this.maxRaidTime(raidGeneralSection.getInt("max-raid-time", 1800));
      this.raidStartCountdown(raidGeneralSection.getInt("raid-start-countdown", 1800));
      this.blindDuringCountdownEnabled(
          raidGeneralSection.getBoolean("blind-during-countdown", true));
      this.friendlyFireEnabled(raidGeneralSection.getBoolean("allow-friendly-fire", true));
      this.taggedModeEnabled(raidGeneralSection.getBoolean("enable-tagged-mode", true));
      this.obituariesEnabled(raidGeneralSection.getBoolean("enable-obituaries", false));
      this.undeadBurnEnabled(raidGeneralSection.getBoolean("enable-undead-burn", true));
      this.dayNightCycleEnabled(raidGeneralSection.getBoolean("enable-day-night-cycle", true));
      this.bossesEnabled(raidGeneralSection.getBoolean("enable-bosses", true));
      this.passiveMobsEnabled(raidGeneralSection.getBoolean("enable-passive-mobs", false));
      this.globalAggroEnabled(raidGeneralSection.getBoolean("enable-global-aggro", false));

      Set<WeatherType> enabledWeatherTypes =
          raidGeneralSection.getStringList("enabled-weather").stream()
              .map(WeatherType::valueOf)
              .collect(Collectors.toSet());

      if (enabledWeatherTypes.isEmpty()) {
        enabledWeatherTypes.addAll(
            List.of(WeatherType.CLEAR, WeatherType.RAIN, WeatherType.THUNDER));
      }

      this.enabledWeatherTypes(enabledWeatherTypes);
    }
  }


}
