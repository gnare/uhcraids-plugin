/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.uhcraids.raid;

import com.sk89q.worldedit.math.BlockVector3;
import com.sk89q.worldedit.regions.Region;
import com.voxelbuster.uhcraids.PluginInterface;
import com.voxelbuster.uhcraids.config.types.RaidBorderMode;
import com.voxelbuster.uhcraids.persistence.IPersistentPluginData;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Stack;
import java.util.function.BiPredicate;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import me.clip.placeholderapi.PlaceholderAPI;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@RequiredArgsConstructor
public class RaidMap implements IPersistentPluginData {
  private final String name;
  private final World raidWorld;
  private final Material worldIcon;
  private final Region worldRegion;
  private final ArrayList<Location> spawnLocations = new ArrayList<>();

  @Getter(AccessLevel.NONE)
  private final ArrayList<ExitRegion> exitRegions = new ArrayList<>();

  private RaidLootPool lootPool = new RaidLootPool();
  private RaidNPCGenerator npcGenerator = new RaidNPCGenerator();
  private boolean distributePartySpawns = false;

  public void addExitRegion(Region r) {
    addExitRegion(r, (region, player) -> true);
  }

  public void addExitRegion(Region r, int exitTimeSec) {
    addExitRegion(r, (region, player) -> true, exitTimeSec);
  }

  public void addExitRegion(Region r, BiPredicate<Region, RaidPlayer> predicate) {
    exitRegions.add(
        new ExitRegion(r, predicate, plugin.getConfig().getInt("raid-exit.default-time", 8)));
  }

  public void addExitRegion(Region r, BiPredicate<Region, RaidPlayer> predicate, int exitTimeSec) {
    exitRegions.add(new ExitRegion(r, predicate, exitTimeSec));
  }

  public boolean removeExitRegion(@NotNull Region r) {
    return exitRegions.removeIf(exitRegion -> exitRegion.region.equals(r));
  }

  public void clearExitRegions() {
    exitRegions.clear();
  }

  public @Nullable ExitRegion getRegionContainingPlayer(RaidPlayer p) {
    Location l = p.getPlayer().getLocation();
    return exitRegions.stream()
        .filter(r -> r.region().contains(BlockVector3.at(l.getX(), l.getY(), l.getZ())))
        .findFirst()
        .orElse(null);
  }

  public boolean canPlayerExit(RaidPlayer p, Region r) {
    Location l = p.getPlayer().getLocation();
    return r.contains(BlockVector3.at(l.getX(), l.getY(), l.getZ()))
        && exitRegions.stream()
        .filter(exitRegion -> exitRegion.region.equals(r))
        .findFirst()
        .orElseThrow()
        .exitCriteria()
        .test(r, p);
  }

  public boolean canPlayerExit(RaidPlayer p) {
    ExitRegion exitRegion = getRegionContainingPlayer(p);
    if (exitRegion == null) {
      return false;
    }
    return exitRegion.exitCriteria().test(exitRegion.region(), p);
  }

  public boolean spawnPlayers(List<RaidPlayer> players, Random rand) {
    if (spawnLocations.size() < players.size()) {
      plugin
          .getLogger()
          .severe(
              "Could not spawn players: "
                  + players.size()
                  + " players > "
                  + spawnLocations.size()
                  + " spawn locations.");
      return false;
    }

    Stack<Location> availableLocations = new Stack<>();
    Collections.copy(availableLocations, spawnLocations);
    Collections.shuffle(availableLocations, rand);

    players.forEach(p -> safeTeleport(p.getPlayer(), availableLocations.pop()));

    return true;
  }

  public boolean spawnPlayers(List<RaidPlayer> players) {
    return spawnPlayers(players, new Random());
  }

  public boolean spawnParties(List<RaidParty> parties, Random rand) {
    if (spawnLocations.size() < parties.size()) {
      plugin
          .getLogger()
          .severe(
              "Could not spawn parties: "
                  + parties.size()
                  + " parties > "
                  + spawnLocations.size()
                  + " spawn locations.");
      return false;
    }

    Stack<Location> availableLocations = new Stack<>();
    Collections.copy(availableLocations, spawnLocations);
    Collections.shuffle(availableLocations, rand);

    parties.forEach(
        party -> {
          Location loc = availableLocations.pop();
          party
              .getPlayers()
              .forEach(
                  player -> {
                    safeTeleport(player, loc.add(Vector.getRandom().multiply(5).setY(0)));
                  });
        });

    return true;
  }

  public boolean spawnParties(List<RaidParty> parties) {
    return spawnParties(parties, new Random());
  }

  @Getter(AccessLevel.NONE)
  @Setter(AccessLevel.NONE)
  private transient PluginInterface plugin = pluginImpl;

  private static final PluginInterface pluginImpl = new PluginInterface() {
  };

  @SuppressWarnings("IntegerDivisionInFloatingPointContext")
  public static boolean safeTeleport(Player player, Location location) {
    if (location.getWorld() == null) {
      pluginImpl
          .getLogger()
          .warning("Could not safely teleport" + player.getName() + " to " + location.toVector());
      return false;
    }
    Block b = location.getWorld().getHighestBlockAt(location);

    for (int attempts = 1; !b.getRelative(BlockFace.DOWN).getType().isSolid() || !b.getRelative(BlockFace.UP)
        .isPassable() || b.getRelative(BlockFace.UP).isLiquid(); attempts++) {
      // This should iterate through coordinates of an archimedean spiral
      b.getWorld()
          .getBlockAt(
              b.getLocation()
                  .add(
                      0.6 * attempts * Math.cos(attempts),
                      0,
                      0.6 * attempts * Math.sin(attempts)));

      if (attempts > 250) {
        pluginImpl
            .getLogger()
            .warning("Could not safely teleport" + player.getName() + " to " + location.toVector());
        return false;
      }
    }

    return player.teleport(b.getLocation().add(0, 0, 0));
  }

  public void playerBoundsCheck(Raid raid, RaidPlayer raidPlayer) {
    if (raid.getPlayers().contains(raidPlayer)) {
      if (raid.getState() == Raid.RaidState.PLAYING) {
        Location playerLoc = raidPlayer.getPlayer().getLocation();
        if (!worldRegion.contains(BlockVector3.at(playerLoc.getX(), playerLoc.getY(), playerLoc.getZ()))) {
          if (raidPlayer.getTimeOutOfBounds() == null) {
            raidPlayer.setTimeOutOfBounds(Instant.now());
          }
          switch (plugin.getConfig().getObject("raid-border.mode", RaidBorderMode.class, RaidBorderMode.TELEPORT)) {
            case KILL:
              if (Duration.between(raidPlayer.getTimeOutOfBounds(), Instant.now()).toSeconds() >= plugin.getConfig()
                  .getInt("raid-border.grace-period", 5)) {
                raidPlayer.getPlayer().damage(Double.MAX_VALUE);
              }
              break;
            case DAMAGE:
              if (Duration.between(raidPlayer.getTimeOutOfBounds(), Instant.now()).toSeconds() >= plugin.getConfig()
                  .getInt("raid-border.grace-period", 5)) {
                raidPlayer.getPlayer().damage(plugin.getConfig().getDouble("raid-border.border-dps", 2));
                raidPlayer.setTimeOutOfBounds(Instant.now());
              }
              break;
            case TELEPORT:
            default:
              movePlayerInBounds(raidPlayer);
              raidPlayer.getPlayer()
                  .sendMessage(NamedTextColor.RED.value() + PlaceholderAPI.setPlaceholders(raidPlayer.getPlayer(),
                      plugin.getConfig().getString("raid-border.teleport-text", "")));
              break;
          }
        } else {
          raidPlayer.setLastInBoundsLocation(raidPlayer.getPlayer().getLocation());
          raidPlayer.setTimeOutOfBounds(null);
        }
      } else {
        raidPlayer.setLastInBoundsLocation(null);
        raidPlayer.setTimeOutOfBounds(null);
      }
    }
  }

  public void movePlayerInBounds(RaidPlayer player) {
    if (player.getLastInBoundsLocation() == null) {
      return;
    }
    player.getPlayer().teleport(player.getLastInBoundsLocation());
  }

  public record ExitRegion(Region region, BiPredicate<Region, RaidPlayer> exitCriteria,
                           int exitTimeSec) implements IPersistentPluginData {
  }
}
