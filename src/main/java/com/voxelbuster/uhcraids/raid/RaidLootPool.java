/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.uhcraids.raid;

import com.voxelbuster.uhcraids.persistence.IPersistentPluginData;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import javax.annotation.Nullable;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Location;
import org.bukkit.block.Container;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
public class RaidLootPool implements IPersistentPluginData {
  private final Hashtable<Location, List<RaidLootDistribution>> groundLootPool = new Hashtable<>();
  private final Hashtable<Container, List<RaidLootDistribution>> containerLootPool =
      new Hashtable<>();

  private transient Random random;

  public RaidLootPool() {
    random = new Random();
  }

  public RaidLootPool(Random random) {
    this.random = random;
  }

  @Nullable
  public ItemStack getLootForLocation(@NotNull Location loc) {
    int currentPriority = 0;
    ItemStack selectedStack = null;
    for (RaidLootDistribution dist : groundLootPool.get(loc)) {
      double rnum = random.nextDouble();
      if (rnum <= dist.probability() && dist.priority() >= currentPriority) {
        int count = random.nextInt(dist.min(), dist.max() + 1);
        currentPriority = dist.priority();
        selectedStack = dist.item().clone();
        selectedStack.setAmount(count);
      }
    }
    return selectedStack;
  }

  public List<ItemStack> getLootForContainer(@NotNull Container container, boolean fitInContainer) {
    LinkedList<PriorityItemStack> loot = new LinkedList<>();
    for (RaidLootDistribution dist : containerLootPool.get(container)) {
      double rnum = random.nextDouble();
      if (rnum <= dist.probability()) {
        int count = random.nextInt(dist.min(), dist.max() + 1);
        ItemStack stack = dist.item().clone();
        stack.setAmount(count);
        loot.push(new PriorityItemStack(dist.priority(), stack));
      }
    }

    return loot.stream()
        .sorted()
        .map(PriorityItemStack::item)
        .collect(Collectors.toList())
        .subList(
            0,
            (fitInContainer)
                ? container.getInventory().getSize()
                : containerLootPool.get(container).size());
  }

  public record RaidLootDistribution(
      double probability, int priority, int min, int max, ItemStack item) implements IPersistentPluginData {}

  private record PriorityItemStack(int priority, ItemStack item)
      implements Comparable<PriorityItemStack> {
    @Override
    public int compareTo(@NotNull RaidLootPool.PriorityItemStack priorityItemStack) {
      return this.priority - priorityItemStack.priority();
    }
  }
}
