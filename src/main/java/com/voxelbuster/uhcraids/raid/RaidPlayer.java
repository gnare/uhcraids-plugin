/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.uhcraids.raid;

import com.voxelbuster.uhcraids.PluginInterface;
import com.voxelbuster.uhcraids.UHCRaidsPlugin;
import com.voxelbuster.uhcraids.persistence.IPersistentPluginData;
import com.voxelbuster.uhcraids.raid.skills.RaidSkills;
import com.voxelbuster.uhcraids.task.DelayedTask;
import java.time.Instant;
import java.util.Date;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

@Getter
@Setter
public class RaidPlayer implements IPersistentPluginData {
  private final transient Player player;

  private final RaidSkills skills;
  private final RaidStats stats;

  private transient Raid currentRaid;
  private transient RaidParty party;

  private transient DelayedTask activeTimedTask;
  private transient Instant timeOutOfBounds;
  private transient Instant timeOfDeath;
  private transient Instant timeOfExit;
  private transient Location lastInBoundsLocation;

  private Inventory outOfRaidInventory;
  private int level;
  private int exp;
  private double hydration;
  private double stamina;

  public RaidPlayer(Player player) throws InvalidConfigurationException {
    this(player, pluginImpl);
  }

  RaidPlayer(Player player, PluginInterface plugin) throws InvalidConfigurationException {
    this.player = player;
    this.plugin = plugin;
    this.outOfRaidInventory =
        plugin.getServer().createInventory(player, 27, player.getName() + "'s Stash");

    ConfigurationSection raidPlayerSection =
        plugin.getConfig().getConfigurationSection("raid-player");
    if (raidPlayerSection == null) {
      throw new InvalidConfigurationException("Missing raid-player section in config!");
    }

    this.skills = RaidSkills.getStartingSkills();
    this.stats = new RaidStats();
    this.level = 1;
    this.exp = 0;
    this.hydration = raidPlayerSection.getDouble("base-hydration", 100d);
    this.stamina = raidPlayerSection.getDouble("base-stamina", 100d);
  }

  public boolean setInventorySize(int slots) {
    int filledSlots = 0;
    for (ItemStack stack : outOfRaidInventory.getContents()) {
      filledSlots += (stack.getAmount() > 0) ? 1 : 0;
    }
    if (slots < filledSlots) {
      return false;
    }

    Inventory newInventory =
        plugin.getServer().createInventory(player, slots, player.getName() + "'s Stash");

    newInventory.setContents(this.outOfRaidInventory.getContents());
    this.outOfRaidInventory = newInventory;
    return true;
  }

  public DelayedTask startExitTimer(long ticks) {
    Date taskTime = Date.from(Instant.now().plusMillis(ticks * 50));
    DelayedTask task =
        new DelayedTask(
            "exitTimer",
            this,
            taskTime,
            () -> {
              exitRaid();
              activeTimedTask = null;
            },
            () -> {
              activeTimedTask = null;
            });
    this.activeTimedTask = task;
    task.start();
    return task;
  }

  private void exitRaid() {
    stats.addRaidsSurvived(1);
    timeOfExit = Instant.now();
  }

  public void onLostInRaid() {
    stats.addDeaths(1);
    // TODO
  }

  public void onKilledInRaid() {
    stats.addDeaths(1);
    timeOfDeath = Instant.now();
    // TODO
  }

  public void onLeaveRaid() {
    stats.addRaidsAbandoned(1);
    // TODO
  }

  public void onStartRaid() {
    stats.addRaids(1);
    timeOutOfBounds = null;
    lastInBoundsLocation = player.getLocation();
    // TODO
  }

  @Getter(AccessLevel.NONE)
  @Setter(AccessLevel.NONE)
  private transient PluginInterface plugin;

  private static final PluginInterface pluginImpl = new PluginInterface() {};

  public static RaidPlayer from(Player player) throws InvalidConfigurationException {
    RaidPlayer rp = UHCRaidsPlugin.getInstance().getPluginState().getRaidPlayersMap().get(player);
    return (rp != null) ? rp : new RaidPlayer(player);
  }

}
