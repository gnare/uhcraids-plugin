/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.uhcraids.raid;

import com.voxelbuster.uhcraids.PluginInterface;
import com.voxelbuster.uhcraids.event.raid.RaidPartyEvent;
import com.voxelbuster.uhcraids.persistence.IPersistentPluginData;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;

@Getter
@Setter
public class RaidParty implements IPersistentPluginData {
  private static final HashMap<Player, RaidParty> partiesByPlayer = new HashMap<>();

  private final HashSet<Player> players = new HashSet<>();
  private final HashSet<Player> invitedPlayers = new HashSet<>();
  private final LocalDateTime createdTimestamp;

  private Player leader;
  private String description = "";

  private boolean open = false;

  public RaidParty(Player leader) {
    this(leader, pluginImpl);
  }

  public RaidParty(Player leader, PluginInterface plugin) {
    this.plugin = plugin;
    this.leader = leader;
    this.createdTimestamp = LocalDateTime.now();

    partiesByPlayer.put(leader, this);
    players.add(leader);
  }

  public boolean setLeader(Player leader) {
    if (!players.contains(leader)) {
      return false;
    }

    plugin
        .getServer()
        .getPluginManager()
        .callEvent(new RaidPartyEvent(leader, this, RaidPartyEvent.Subtype.TRANSFER_LEADERSHIP));
    partiesByPlayer.remove(this.leader);
    partiesByPlayer.put(leader, this);
    this.leader = leader;
    return true;
  }

  public boolean invitePlayer(Player player) {
    plugin
        .getServer()
        .getPluginManager()
        .callEvent(new RaidPartyEvent(player, this, RaidPartyEvent.Subtype.INVITE));
    return invitedPlayers.add(player);
  }

  public boolean acceptInvite(Player acceptedPlayer) {
    if (invitedPlayers.remove(acceptedPlayer)) {
      plugin
          .getServer()
          .getPluginManager()
          .callEvent(new RaidPartyEvent(acceptedPlayer, this, RaidPartyEvent.Subtype.JOIN));
      return players.add(acceptedPlayer);
    }
    plugin
        .getServer()
        .getPluginManager()
        .callEvent(new RaidPartyEvent(acceptedPlayer, this, RaidPartyEvent.Subtype.JOIN_FAIL));
    return false;
  }

  public boolean kickPlayer(Player player) {
    plugin
        .getServer()
        .getPluginManager()
        .callEvent(new RaidPartyEvent(player, this, RaidPartyEvent.Subtype.KICK));
    return players.remove(player) || invitedPlayers.remove(player);
  }

  public boolean leave(Player playerLeaving) {
    plugin
        .getServer()
        .getPluginManager()
        .callEvent(new RaidPartyEvent(playerLeaving, this, RaidPartyEvent.Subtype.LEAVE));
    return players.remove(playerLeaving);
  }

  public boolean tryJoin(Player joining) {
    if (open) {
      plugin
          .getServer()
          .getPluginManager()
          .callEvent(new RaidPartyEvent(joining, this, RaidPartyEvent.Subtype.JOIN));
      return players.add(joining);
    } else {
      if (acceptInvite(joining)) {
        plugin
            .getServer()
            .getPluginManager()
            .callEvent(new RaidPartyEvent(joining, this, RaidPartyEvent.Subtype.JOIN));
        return true;
      }
      plugin
          .getServer()
          .getPluginManager()
          .callEvent(new RaidPartyEvent(joining, this, RaidPartyEvent.Subtype.JOIN_FAIL));
      return false;
    }
  }

  public void disband() {
    RaidPartyEvent e = new RaidPartyEvent(leader, this, RaidPartyEvent.Subtype.DISBAND);
    plugin.getServer().getPluginManager().callEvent(e);
    if (e.isCancelled()) {
      return;
    }

    players.clear();
    invitedPlayers.clear();
    partiesByPlayer.remove(leader);
    leader = null;
    open = false;
  }

  public static RaidParty getPartyByLeader(Player player) {
    return partiesByPlayer.get(player);
  }

  public Collection<Player> getPlayers() {
    return players.stream().toList();
  }

  public Collection<Player> getInvitedPlayers() {
    return invitedPlayers.stream().toList();
  }

  public static List<RaidParty> getPartiesByDesc(String search) {
    return partiesByPlayer.values().stream()
        .filter(party -> party.getDescription().toLowerCase().contains(search.toLowerCase()))
        .collect(Collectors.toList());
  }

  @Getter(AccessLevel.NONE)
  @Setter(AccessLevel.NONE)
  private transient PluginInterface plugin;

  private static final PluginInterface pluginImpl = new PluginInterface() {};
}
