/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.uhcraids.raid;

import com.voxelbuster.uhcraids.PluginInterface;
import com.voxelbuster.uhcraids.persistence.IPersistentPluginData;
import java.sql.Date;
import java.time.Instant;
import java.util.HashSet;
import java.util.Timer;
import java.util.TimerTask;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import net.kyori.adventure.text.format.NamedTextColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

@Getter
public class Raid implements IPersistentPluginData {
  private final HashSet<RaidParty> parties = new HashSet<>();
  private final HashSet<RaidPlayer> players = new HashSet<>();

  private final RaidSettings settings;

  private final RaidNPCGenerator npcGenerator;

  private Instant startTime, endTime;

  private RaidState state = RaidState.INACTIVE;

  public Raid(RaidSettings settings, RaidNPCGenerator npcGenerator) {
    this(settings, npcGenerator, pluginImpl);
  }

  Raid(RaidSettings settings, RaidNPCGenerator npcGenerator, PluginInterface plugin) {
    this.plugin = plugin;
    this.settings = settings;
    this.npcGenerator = npcGenerator;
  }

  public void matchmake() {
    this.state = RaidState.MATCHMAKING;
    // TODO
  }

  public void start() {
    this.state = RaidState.STARTING;

    settings.getRaidMap().spawnParties(parties.stream().toList());

    players.forEach(
        raidPlayer -> {
          raidPlayer
              .getPlayer()
              .spigot()
              .sendMessage(
                  ChatMessageType.ACTION_BAR,
                  new TextComponent(NamedTextColor.GOLD.value() + "Raid starting in: ")); // TODO
        });

    this.startTime = Instant.now();
    this.endTime = startTime.plusSeconds(settings.getMaxRaidTime());

    players.forEach(raidPlayer -> raidPlayer.setCurrentRaid(this));

    TimerTask endRaidTask =
        new TimerTask() {
          @Override
          public void run() {
            Raid.this.end();
          }
        };
    Timer raidEndTimer = new Timer();
    raidEndTimer.schedule(
        endRaidTask,
        Date.from(endTime));

    this.state = RaidState.PLAYING;
  }

  public void end() {
    this.state = RaidState.SAVING;
    // TODO cleanup sequence
    players.stream()
        .filter(raidPlayer -> raidPlayer.getCurrentRaid() == this)
        .forEach(RaidPlayer::onLostInRaid);

    this.state = RaidState.ENDED;
  }

  public int getAlivePlayers() {
    return (int) getPlayers().stream()
        .filter(raidPlayer -> !raidPlayer.getPlayer().isDead() && raidPlayer.getCurrentRaid() == this).count();
  }

  @Getter(AccessLevel.NONE)
  @Setter(AccessLevel.NONE)
  private transient PluginInterface plugin;

  private static final PluginInterface pluginImpl = new PluginInterface() {
  };

  public enum RaidState {
    INACTIVE,
    MATCHMAKING,
    STARTING,
    PLAYING,
    SAVING,
    ENDED
  }
}
