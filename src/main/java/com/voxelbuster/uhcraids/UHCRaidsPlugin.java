/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.uhcraids;

import com.comphenix.packetwrapper.WrapperPlayServerPlayerInfo;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.EnumWrappers;
import com.comphenix.protocol.wrappers.PlayerInfoData;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.samjakob.spigui.SpiGUI;
import com.sk89q.worldguard.WorldGuard;
import com.voxelbuster.uhcraids.commands.UHCRaidsCommand;
import com.voxelbuster.uhcraids.config.types.StorageType;
import com.voxelbuster.uhcraids.persistence.DummyDataHandler;
import com.voxelbuster.uhcraids.persistence.FirebaseDataHandler;
import com.voxelbuster.uhcraids.persistence.FlatFileDataHandler;
import com.voxelbuster.uhcraids.persistence.MongoDataHandler;
import com.voxelbuster.uhcraids.persistence.PlayerDataManager;
import com.voxelbuster.uhcraids.persistence.RedisDataHandler;
import com.voxelbuster.uhcraids.placeholders.UHCRaidsExpansion;
import com.voxelbuster.uhcraids.raid.Raid;
import com.voxelbuster.uhcraids.raid.RaidPlayer;
import com.voxelbuster.uhcraids.raid.RaidSettings;
import java.io.IOException;
import java.lang.reflect.Type;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import lombok.AccessLevel;
import lombok.Getter;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.text.format.TextDecoration;
import net.milkbowl.vault.Vault;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.apache.commons.collections4.list.TreeList;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.PluginCommand;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

@Getter
public class UHCRaidsPlugin extends JavaPlugin {
  public static final String pluginName = "UHCRaids";

  @Getter private static UHCRaidsPlugin instance;

  @Getter
  private static final ExecutorService executor =
      new ThreadPoolExecutor(
          0, // TODO possibly allow tweaking of this value in config?
          Runtime.getRuntime().availableProcessors(),
          60L,
          TimeUnit.SECONDS,
          new SynchronousQueue<>());

  @Getter(AccessLevel.NONE)
  private final Logger log = super.getLogger();

  private final WorldGuard worldGuard = WorldGuard.getInstance();

  private PlayerDataManager playerDataManager = new PlayerDataManager(new DummyDataHandler());

  private final PluginState pluginState = new PluginState();

  private SpiGUI spiGUI;

  @Getter(AccessLevel.PROTECTED)
  private Economy economy;

  @Getter(AccessLevel.PROTECTED)
  private Chat chat;

  @Getter(AccessLevel.PROTECTED)
  private Permission permissions;

  private Vault vault;
  private ProtocolManager protocolManager;

  @Override
  public void onEnable() {
    super.onEnable();

    instance = this;
    log.info("Starting UHCRaids Plugin.");

    // Make sure the server is actually a paper/spigot server (crash and disable plugin if not)
    getServer().spigot();
    assert getServer().dispatchCommand(getServer().getConsoleSender(), "paper");

    //noinspection ResultOfMethodCallIgnored
    Objects.requireNonNull(getServer().getPluginManager().getPlugin(UHCRaidsPlugin.pluginName))
        .getDataFolder()
        .mkdirs();

    protocolManager = ProtocolLibrary.getProtocolManager();
    spiGUI = new SpiGUI(this);

    UHCRaidsCommand pbc = new UHCRaidsCommand("uhcraids");
    PluginCommand pluginCommand = this.getCommand("uhcraids");

    if (pluginCommand == null) {
      log.severe("Could not bind command!");
      throw new IllegalStateException();
    }

    pluginCommand.setExecutor(pbc);
    pluginCommand.setTabCompleter(pbc);

    Plugin vaultPluginTmp = this.getServer().getPluginManager().getPlugin("Vault");
    if (vaultPluginTmp instanceof Vault) {
      vault = (Vault) vaultPluginTmp;
      log.info("Vault API hooked.");

      if (!setupEconomy()) {
        log.severe("Setting up economy failed");
      }

      if (!setupPermissions()) {
        log.severe("Setting up permission manager failed");
      }

      if (!setupChat()) {
        log.severe("Setting up chat manager failed");
      }
    }

    if (this.getServer().getPluginManager().getPlugin("PlaceholderAPI") != null) {
      new UHCRaidsExpansion().register();
      log.info("Placeholder expansions registered.");
    }

    getConfig(); // Preload config

    List<String> storageMethods = getConfig().getStringList("storage");
    if (storageMethods.isEmpty()) {
      log.severe(
          "ATTENTION!!! Config key 'storage' has no values! This means plugin data will not be saved!");
    } else {
      switch (StorageType.valueOf(storageMethods.get(0))) {
        case FILETREE -> playerDataManager = new PlayerDataManager(new FlatFileDataHandler());
        case FIREBASE -> playerDataManager = new PlayerDataManager(new FirebaseDataHandler());
        case REDIS -> playerDataManager = new PlayerDataManager(new RedisDataHandler());
        case MONGODB -> playerDataManager = new PlayerDataManager(new MongoDataHandler());
        default -> log.severe(
            "ATTENTION!!! Config key 'storage' has an invalid value! This means plugin data will not be saved!");
      }
    }

    startPeriodicTasks();
    setupPacketListeners();
  }

  private boolean setupEconomy() {
    if (vault == null) {
      return false;
    }

    RegisteredServiceProvider<Economy> rsp =
        getServer().getServicesManager().getRegistration(Economy.class);
    if (rsp == null) {
      return false;
    }
    economy = rsp.getProvider();
    return true;
  }

  private boolean setupPermissions() {
    if (vault == null) {
      return false;
    }

    RegisteredServiceProvider<Permission> rsp =
        getServer().getServicesManager().getRegistration(Permission.class);
    if (rsp == null) {
      return false;
    }
    permissions = rsp.getProvider();
    return true;
  }

  private boolean setupChat() {
    if (vault == null) {
      return false;
    }

    RegisteredServiceProvider<Chat> rsp =
        getServer().getServicesManager().getRegistration(Chat.class);
    if (rsp == null) {
      return false;
    }
    chat = rsp.getProvider();
    return true;
  }

  @Override
  public void onDisable() {
    super.onDisable();
    log.info("Disabling UHCRaids Plugin.");
  }

  private void setupPacketListeners() {
    protocolManager.addPacketListener(
        new PacketAdapter(this, ListenerPriority.NORMAL, PacketType.Play.Server.PLAYER_INFO) {
          @Override
          public void onPacketSending(PacketEvent event) {
            RaidPlayer raidPlayer = getPluginState().getRaidPlayersMap().get(event.getPlayer());
            if (raidPlayer != null
                && raidPlayer.getCurrentRaid() != null
                && !event
                    .getPacket()
                    .getPlayerInfoDataLists()
                    .getValues()
                    .get(0)
                    .get(0)
                    .getProfile()
                    .getName()
                    .equals("uhcraids_list_header")) {
              event.setCancelled(true);
              WrapperPlayServerPlayerInfo playerInfoWrapper = new WrapperPlayServerPlayerInfo();
              int playersAlive = raidPlayer.getCurrentRaid().getAlivePlayers();
              long mins =
                  Duration.between(raidPlayer.getCurrentRaid().getEndTime(), Instant.now())
                          .toSeconds()
                      / 60L;
              long sec =
                  Duration.between(raidPlayer.getCurrentRaid().getEndTime(), Instant.now())
                          .toSeconds()
                      % 60L;
              playerInfoWrapper.setData(
                  List.of(
                      new PlayerInfoData(
                          new WrappedGameProfile((UUID) null, "uhcraids_list_header"),
                          0,
                          EnumWrappers.NativeGameMode.SURVIVAL,
                          WrappedChatComponent.fromText(
                              TextDecoration.BOLD
                                  + "Playing on map: "
                                  + raidPlayer
                                      .getCurrentRaid()
                                      .getSettings()
                                      .getRaidMap()
                                      .getName())),
                      new PlayerInfoData(
                          new WrappedGameProfile((UUID) null, "players_alive"),
                          0,
                          EnumWrappers.NativeGameMode.SURVIVAL,
                          WrappedChatComponent.fromText(
                              NamedTextColor.GREEN.value() + "Players alive: " + playersAlive)),
                      new PlayerInfoData(
                          new WrappedGameProfile((UUID) null, "time_left"),
                          0,
                          EnumWrappers.NativeGameMode.SURVIVAL,
                          WrappedChatComponent.fromText(
                              NamedTextColor.GOLD.value() + String.format("Time left: %02d:%02d", mins, sec)))));

              playerInfoWrapper.sendPacket(raidPlayer.getPlayer());
              // TODO send packets for raid info in player list
            }
          }
        });
  }

  public static Gson getGson() {
    HashMap<Type, TypeAdapter<?>> typeAdapters = new HashMap<>();
    typeAdapters.putAll(
        Map.of(
            Inventory.class,
            new TypeAdapter<Inventory>() {
              @Override
              public void write(JsonWriter out, Inventory value) throws IOException {
                out.beginObject();

                out.name("type");
                out.value(value.getType().toString());

                if (value.getLocation() != null) {
                  out.name("location");
                  out.jsonValue(new Gson().toJson(value.getLocation().serialize()));
                }

                out.name("size");
                out.value(value.getSize());

                out.name("contents");
                out.beginArray();
                for (ItemStack is : value.getContents()) {
                  out.jsonValue(new Gson().toJson(is));
                }
                out.endArray();

                out.endObject();
              }

              @Override
              public Inventory read(JsonReader in) throws IOException {
                in.beginObject();

                String type = "CHEST";
                String location;
                int size = 27;
                ItemStack[] stacks = new ItemStack[0];

                while (in.hasNext()) {
                  String name = in.nextName();
                  switch (name) {
                    case "type":
                      type = in.nextString();
                      break;
                    case "location":
                      location = in.nextString();
                      break;
                    case "size":
                      size = in.nextInt();
                      break;
                    case "contents":
                      stacks =
                          (ItemStack[])
                              new Gson().fromJson(in, TypeToken.getArray(ItemStack.class));
                    default:
                      break;
                  }
                }

                in.endObject();

                Inventory i = getInstance().getServer().createInventory(null, size);
                i.setContents(stacks);

                in.endObject();
                return i;
              }
            }));

    GsonBuilder gb = new GsonBuilder().setPrettyPrinting();
    typeAdapters.forEach(gb::registerTypeAdapter);
    return gb.create();
  }

  private void startPeriodicTasks() {
    Bukkit.getScheduler()
        .runTaskTimer(
            this,
            () -> {
              getPluginState()
                  .getActiveRaids()
                  .forEach(
                      raid -> {
                        executor.submit(
                            () -> {
                              raid.getPlayers()
                                  .forEach(
                                      raidPlayer -> {
                                        if (raid.getSettings()
                                            .getRaidMap()
                                            .canPlayerExit(raidPlayer)) {
                                          if (raidPlayer.getActiveTimedTask() == null) {
                                            raidPlayer.startExitTimer(
                                                20L
                                                    * Objects.requireNonNull(
                                                            raid.getSettings()
                                                                .getRaidMap()
                                                                .getRegionContainingPlayer(
                                                                    raidPlayer))
                                                        .exitTimeSec());
                                          }
                                        } else {
                                          if (raidPlayer.getActiveTimedTask() != null
                                              && raidPlayer
                                                  .getActiveTimedTask()
                                                  .getName()
                                                  .equals("exitTimer")) {
                                            raidPlayer.getActiveTimedTask().cancel();
                                          }
                                        }
                                      });
                            });
                      });
            },
            40L,
            5L);

    Bukkit.getScheduler()
        .runTaskTimer(
            this,
            () -> {
              // TODO update plugin HUDs
            },
            40L,
            getConfig().getInt("title-update-interval", 2));

    Bukkit.getScheduler()
        .runTaskTimer(
            this,
            () -> {
              getPluginState()
                  .getActiveRaids()
                  .forEach(
                      raid -> {
                        executor.submit(
                            () -> {
                              raid.getPlayers()
                                  .forEach(
                                      player ->
                                          raid.getSettings()
                                              .getRaidMap()
                                              .playerBoundsCheck(raid, player));
                            });
                      });
            },
            40L,
            20L);
  }

  @Override
  public void reloadConfig() {
    super.reloadConfig();
  }

  @Getter
  public static class PluginState {
    private final HashMap<OfflinePlayer, RaidPlayer> raidPlayersMap = new HashMap<>();
    private final TreeList<Raid> activeRaids = new TreeList<>();
    private final HashMap<String, RaidSettings> raidSettingsMap = new HashMap<>();

    private final TreeList<RaidPlayer> playersInMatchmaking = new TreeList<>();
  }
}
