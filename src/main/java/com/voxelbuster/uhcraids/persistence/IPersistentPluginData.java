/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.uhcraids.persistence;

import com.google.gson.Gson;
import com.voxelbuster.uhcraids.PluginInterface;
import java.io.Serializable;

public interface IPersistentPluginData extends Serializable {
  default String serialize() {
      Gson gson = new PluginInterface(){}.getGson();
      return gson.toJson(this);
  }

  default IPersistentPluginData deserialize(String json) {
      return deserialize(json, getClass());
  }

  static IPersistentPluginData deserialize(String json, Class<? extends IPersistentPluginData> clazz) {
    Gson gson = new PluginInterface(){}.getGson();
    return gson.fromJson(json, clazz);
  }
}
