/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.uhcraids.persistence;

import com.voxelbuster.uhcraids.raid.RaidPlayer;
import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.InvalidConfigurationException;

public class PlayerDataManager {

    private final IDataHandler dataHandler;

    public PlayerDataManager(IDataHandler dataHandler) {
        this.dataHandler = dataHandler;
    }

    public RaidPlayer loadPlayer(OfflinePlayer player) {
        try {
            return loadPlayer(player.getUniqueId());
        } catch (InvalidConfigurationException e) {
            throw new RuntimeException(e);
        }
    }

    public RaidPlayer loadPlayer(UUID playerUuid) throws InvalidConfigurationException {
        RaidPlayer raidPlayer = (RaidPlayer) dataHandler.read("raidPlayers." + playerUuid.toString());
        if (raidPlayer == null) {
            raidPlayer = RaidPlayer.from(Bukkit.getPlayer(playerUuid));
            dataHandler.write(raidPlayer);
        }

        return raidPlayer;
    }

}
