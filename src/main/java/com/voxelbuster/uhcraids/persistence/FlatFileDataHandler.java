/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.voxelbuster.uhcraids.persistence;

import com.google.common.io.Files;
import com.voxelbuster.uhcraids.PluginInterface;
import com.voxelbuster.uhcraids.UHCRaidsPlugin;
import com.voxelbuster.uhcraids.raid.RaidPlayer;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter
public class FlatFileDataHandler implements IDataHandler {
  private final File dataFolder;
  private final File raidPlayersFolder;
  private final File raidMapsFolder;
  private final File raidLootPoolsFolder;
  private final File raidNPCGeneratorFolder;

  public FlatFileDataHandler() {
    this(pluginImpl);
  }

  FlatFileDataHandler(PluginInterface plugin) {
    this.plugin = plugin;
    this.dataFolder = Objects.requireNonNull(plugin.getServer().getPluginManager().getPlugin(UHCRaidsPlugin.pluginName))
            .getDataFolder();
    this.raidPlayersFolder = Path.of(dataFolder.getAbsolutePath(), "raidPlayers").toFile();
    //noinspection ResultOfMethodCallIgnored
    this.raidPlayersFolder.mkdir();

    this.raidMapsFolder = Path.of(dataFolder.getAbsolutePath(), "raidMaps").toFile();
    //noinspection ResultOfMethodCallIgnored
    this.raidMapsFolder.mkdir();

    this.raidLootPoolsFolder = Path.of(dataFolder.getAbsolutePath(), "raidLootPools").toFile();
    //noinspection ResultOfMethodCallIgnored
    this.raidLootPoolsFolder.mkdir();

    this.raidNPCGeneratorFolder = Path.of(dataFolder.getAbsolutePath(), "raidNpcGens").toFile();
    //noinspection ResultOfMethodCallIgnored
    this.raidNPCGeneratorFolder.mkdir();
  }

  @Override
  public void write(IPersistentPluginData persistentData) {
    if (persistentData instanceof RaidPlayer raidPlayerData) {
      UHCRaidsPlugin.getExecutor().submit(() -> {
        try {
          Files.write(raidPlayerData.serialize().getBytes(StandardCharsets.UTF_8),
                  Path.of(raidPlayersFolder.getAbsolutePath(),
                          raidPlayerData.getPlayer().getUniqueId() + ".json")
                          .toFile());
        } catch (IOException e) {
          plugin.getLogger().log(Level.SEVERE, "Failed to write player " + raidPlayerData.getPlayer().getName()
                  + " (" + raidPlayerData.getPlayer().getUniqueId() + ")", e);
        }
      });
      plugin.getLogger().fine("Submitted " + raidPlayerData.getPlayer().getUniqueId() + "save task.");
    }
  }

  @Override
  public IPersistentPluginData read(String key) {
    ArrayList<String> keySlices = (ArrayList<String>) List.of(key.split("\\."));
    return switch (keySlices.get(0)) {
      case "raidPlayers" -> {
        try {
          yield IPersistentPluginData.deserialize(readFile(Paths.get(raidPlayersFolder.getAbsolutePath(), keySlices.get(1) + ".json").toFile()), RaidPlayer.class);
        } catch (IOException e) {
          plugin.getLogger().log(Level.SEVERE, "Failed to read player " +
                  Paths.get(raidPlayersFolder.getAbsolutePath(), keySlices.get(1) + ".json"), e);
        }
        yield null;
      }
      case "raidMaps" -> null;
      case "raidLootPools" -> null;
      case "raidNpcGens" -> null;
      default -> null;
    };
  }

  @Override
  public String getKeyForObject(IPersistentPluginData persistentData) {
    if (persistentData instanceof RaidPlayer raidPlayerData) {
      return "raidPlayers." + raidPlayerData.getPlayer().getUniqueId();
    }

     return null;
  }

  @Override
  public boolean ready() {
    return true;
  }

  @Override
  public void close() {

  }

  private static String readFile(File file) throws IOException {
    BufferedReader reader = Files.newReader(file, StandardCharsets.UTF_8);
    StringBuilder sb = new StringBuilder();
    reader.lines().forEach(sb::append);

    reader.close();
    return sb.toString();
  }

  @Getter(AccessLevel.NONE)
  @Setter(AccessLevel.NONE)
  private transient PluginInterface plugin;

  private static final PluginInterface pluginImpl = new PluginInterface() {
  };
}
