/*
 * UHCRaids Plugin
 *     Copyright (C) 2023  Galen Nare
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.voxelbuster.uhcraids.event.raid

import com.voxelbuster.uhcraids.raid.Raid
import com.voxelbuster.uhcraids.raid.RaidPlayer
import org.bukkit.event.Cancellable
import org.bukkit.event.Event
import org.bukkit.event.HandlerList
import org.bukkit.event.entity.PlayerDeathEvent
import java.time.Instant

data class RaidPlayerDeathEvent(
    val linkedEvent: PlayerDeathEvent,
    val raidPlayer: RaidPlayer,
    val raid: Raid,
    val deathTime: Instant
) : Event(), Cancellable {
    private val handlers = HandlerList()
    private var cancelled = false
    override fun getHandlers(): HandlerList {
        return handlers
    }

    override fun isCancelled(): Boolean {
        return cancelled
    }

    override fun setCancelled(b: Boolean) {
        linkedEvent.isCancelled = b
        cancelled = b
    }
}
